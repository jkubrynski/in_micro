package pl.ing.fraud;

class FraudStatus {

	private final String clientId;
	private final String fraudStatus;
	private final String matchedRuleId = "27z";
	
	FraudStatus(String clientId, String fraudStatus) {
		this.clientId = clientId;
		this.fraudStatus = fraudStatus;
	}

	public String getClientId() {
		return clientId;
	}

	public String getFraudStatus() {
		return fraudStatus;
	}

	public String getMatchedRuleId() {
		return matchedRuleId;
	}

	public String getCheckResult() {
		return fraudStatus;
	}
}
