package pl.ing.fraud;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/checks")
class FraudCheckController {

	private final FraudCheckService fraudCheckService;

	FraudCheckController(FraudCheckService fraudCheckService) {
		this.fraudCheckService = fraudCheckService;
	}

	@GetMapping(value = "/{clientId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	FraudStatus checkFraud(@PathVariable String clientId) {
		return fraudCheckService.checkStatus(clientId);
	}
}
