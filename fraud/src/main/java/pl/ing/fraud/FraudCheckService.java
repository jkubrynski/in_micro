package pl.ing.fraud;

import org.springframework.stereotype.Service;

@Service
class FraudCheckService {

	FraudStatus checkStatus(String clientId) {
		return new FraudStatus(clientId, "OK");
	}
}
