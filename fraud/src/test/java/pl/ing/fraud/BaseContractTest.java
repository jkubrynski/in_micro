package pl.ing.fraud;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.mockito.Mockito;

abstract class BaseContractTest {

	@Before
	public void setUp() {
		FraudCheckService fraudCheckService = Mockito.mock(FraudCheckService.class);

		Mockito.when(fraudCheckService.checkStatus("1")).thenReturn(new FraudStatus("1", "OK"));

		RestAssuredMockMvc.standaloneSetup(new FraudCheckController(fraudCheckService));
	}
}
