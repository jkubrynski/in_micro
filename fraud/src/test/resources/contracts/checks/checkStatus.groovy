package contracts.checks

import org.springframework.cloud.contract.spec.Contract
import org.springframework.cloud.contract.spec.internal.HttpMethods

Contract.make {
	request {
		url('/checks/1')
		method(HttpMethods.HttpMethod.GET)
		headers {
			accept(applicationJsonUtf8())
		}
	}

	response {
		status(OK())
		headers {
			contentType(applicationJsonUtf8())
		}
		body(
			'clientId': 1,
			'checkResult': 'OK',
			'matchedRuleId' : '27z'
		)
	}
}