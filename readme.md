Dobra architektura:
+ łatwo implementowalna
+ elastyczna
+ dopasowana
+ ustandaryzowana
+ bezpieczna
+ niezawodna + odporna + dostępna
+ przejrzysta
+ testowalna
+ skalowalna

https://c4model.com/
https://structurizr.com/
https://github.com/structurizr/java

Hexagonal arch - https://www.youtube.com/watch?v=ma15iBQpmHU

https://www.amazon.com/Working-Effectively-Legacy-Michael-Feathers/dp/0131177052

https://chriskiehl.com/article/event-sourcing-is-hard

https://www.youtube.com/watch?v=3eCSYexlf8M

http://www.kubrynski.com/2017/07/overview-of-circuit-breaker-in-hystrix.html

User strories considered harmful - https://www.youtube.com/watch?v=ATZ0GEMSivM

https://softwaremill.com/mqperf/
