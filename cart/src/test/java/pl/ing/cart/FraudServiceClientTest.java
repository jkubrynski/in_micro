package pl.ing.cart;

import org.junit.Test;
import org.junit.runner.RunWith;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureStubRunner(ids = "pl.ing:fraud:+:stubs")
public class FraudServiceClientTest {

	@Autowired
	FraudServiceClient fraudServiceClient;

	@Test
	public void shouldInvokeFraudCheck() {
		FraudStatus fraudStatus = fraudServiceClient.checkFraudStatus("1");

		assertThat(fraudStatus.getClientId()).isEqualTo("1");
		assertThat(fraudStatus.getCheckResult()).isEqualTo("OK");
		assertThat(fraudStatus.getMatchedRuleId()).isEqualTo("27z");
	}
}