package pl.ing.cart;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/carts")
class CartController {

	private final CartService cartService;

	CartController(CartService cartService) {
		this.cartService = cartService;
	}

	@GetMapping("/{clientId}")
	String submitCart(@PathVariable String clientId) {
		return cartService.submitCart(clientId);
	}
}
