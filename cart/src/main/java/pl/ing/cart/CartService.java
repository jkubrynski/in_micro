package pl.ing.cart;

import org.springframework.stereotype.Service;

@Service
class CartService {

	private final FraudServiceClient fraudServiceClient;

	CartService(FraudServiceClient fraudServiceClient) {
		this.fraudServiceClient = fraudServiceClient;
	}

	String submitCart(String clientId) {
		FraudStatus checkFraudStatus = fraudServiceClient.checkFraudStatus(clientId);
		if ("OK".equalsIgnoreCase(checkFraudStatus.getCheckResult())) {
			return "SUBMITTED";
		} else {
			return checkFraudStatus.getCheckResult();
		}
	}
}
