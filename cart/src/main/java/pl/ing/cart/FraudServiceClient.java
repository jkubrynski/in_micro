package pl.ing.cart;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "fraud", fallback = FraudServiceClientFallback.class)
interface FraudServiceClient {

	@GetMapping(value = "/checks/{clientId}", produces = MediaType.APPLICATION_JSON_UTF8_VALUE)
	FraudStatus checkFraudStatus(@PathVariable("clientId") String clientId);

}
