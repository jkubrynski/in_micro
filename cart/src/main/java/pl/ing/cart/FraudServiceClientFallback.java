package pl.ing.cart;

import java.lang.invoke.MethodHandles;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;

@Service
class FraudServiceClientFallback implements FraudServiceClient {

	private static final Logger LOG = LoggerFactory.getLogger(MethodHandles.lookup().lookupClass());

	@Override
	public FraudStatus checkFraudStatus(String clientId) {
		LOG.warn("Cannot check fraud status - fallback to async processing for [clientId={}]", clientId);
		return new FraudStatus(clientId, "ASYNC_PROCESSING", clientId);
	}
}
