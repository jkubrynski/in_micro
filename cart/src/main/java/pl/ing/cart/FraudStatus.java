package pl.ing.cart;

class FraudStatus {

	private final String clientId;

	private final String checkResult;

	private final String matchedRuleId;

	public FraudStatus(String clientId, String status, String matchedRuleId) {
		this.clientId = clientId;
		this.checkResult = status;
		this.matchedRuleId = matchedRuleId;
	}

	public String getClientId() {
		return clientId;
	}

	public String getCheckResult() {
		return checkResult;
	}

	public String getMatchedRuleId() {
		return matchedRuleId;
	}
}
